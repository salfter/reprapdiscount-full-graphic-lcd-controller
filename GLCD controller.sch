EESchema Schematic File Version 4
LIBS:GLCD controller-cache
EELAYER 26 0
EELAYER END
$Descr A 11000 8500
encoding utf-8
Sheet 1 1
Title "GLCD controler-SchDoc"
Date "16 08 2018"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
NoConn ~ 3200 2100
NoConn ~ 3200 2000
NoConn ~ 3200 2200
NoConn ~ 3200 2300
$Comp
L power:GND #GND0101
U 1 1 5B7596C1
P 1700 2800
F 0 "#GND0101" H 1700 2940 20  0001 C CNN
F 1 "GND" H 1700 2910 30  0001 C CNN
F 2 "" H 1700 2800 70  0000 C CNN
F 3 "" H 1700 2800 70  0000 C CNN
	1    1700 2800
	1    0    0    -1  
$EndComp
$Comp
L GLCD-controler-SchDoc-rescue:VCC #VCC0101
U 1 1 5B7596C0
P 1200 1300
F 0 "#VCC0101" H 1200 1300 20  0001 C CNN
F 1 "VCC" H 1200 1450 30  0000 C CNN
F 2 "" H 1200 1300 70  0000 C CNN
F 3 "" H 1200 1300 70  0000 C CNN
	1    1200 1300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #GND_02
U 1 1 5B7596BF
P 1950 4850
F 0 "#GND_02" H 1950 4990 20  0001 C CNN
F 1 "GND" H 1950 4960 30  0001 C CNN
F 2 "" H 1950 4850 70  0000 C CNN
F 3 "" H 1950 4850 70  0000 C CNN
	1    1950 4850
	1    0    0    -1  
$EndComp
NoConn ~ 8200 2500
NoConn ~ 8200 2800
NoConn ~ 8200 2400
Wire Wire Line
	7900 2000 8200 2000
Wire Wire Line
	7900 1500 7900 2000
Wire Wire Line
	8000 1900 8200 1900
Wire Wire Line
	8000 2200 8200 2200
$Comp
L GLCD-controler-SchDoc-rescue:3V3 #3V03
U 1 1 5B7596BE
P 7900 1500
F 0 "#3V03" H 7900 1500 20  0001 C CNN
F 1 "3V3" H 7900 1650 30  0000 C CNN
F 2 "" H 7900 1500 70  0000 C CNN
F 3 "" H 7900 1500 70  0000 C CNN
	1    7900 1500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #GND_03
U 1 1 5B7596BD
P 8000 3100
F 0 "#GND_03" H 8000 3240 20  0001 C CNN
F 1 "GND" H 8000 3210 30  0001 C CNN
F 2 "" H 8000 3100 70  0000 C CNN
F 3 "" H 8000 3100 70  0000 C CNN
	1    8000 3100
	1    0    0    -1  
$EndComp
$Comp
L GLCD-controler-SchDoc-rescue:3V3 #3V3_02
U 1 1 5B7596BB
P 8450 3700
F 0 "#3V3_02" H 8450 3700 20  0001 C CNN
F 1 "3V3" H 8450 3850 30  0000 C CNN
F 2 "" H 8450 3700 70  0000 C CNN
F 3 "" H 8450 3700 70  0000 C CNN
	1    8450 3700
	1    0    0    -1  
$EndComp
$Comp
L GLCD-controler-SchDoc-rescue:VCC #VCC_02
U 1 1 5B7596B9
P 1550 5600
F 0 "#VCC_02" H 1550 5600 20  0001 C CNN
F 1 "VCC" H 1550 5750 30  0000 C CNN
F 2 "" H 1550 5600 70  0000 C CNN
F 3 "" H 1550 5600 70  0000 C CNN
	1    1550 5600
	1    0    0    -1  
$EndComp
$Comp
L GLCD-controler-SchDoc-rescue:3V3 #3V3_03
U 1 1 5B7596B8
P 2750 5600
F 0 "#3V3_03" H 2750 5600 20  0001 C CNN
F 1 "3V3" H 2750 5750 30  0000 C CNN
F 2 "" H 2750 5600 70  0000 C CNN
F 3 "" H 2750 5600 70  0000 C CNN
	1    2750 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 5700 1550 5600
Wire Wire Line
	1750 5700 1550 5700
Wire Wire Line
	2750 5700 2550 5700
Wire Wire Line
	2750 5600 2750 5700
$Comp
L power:GND #GND_06
U 1 1 5B7596B7
P 2050 6200
F 0 "#GND_06" H 2050 6340 20  0001 C CNN
F 1 "GND" H 2050 6310 30  0001 C CNN
F 2 "" H 2050 6200 70  0000 C CNN
F 3 "" H 2050 6200 70  0000 C CNN
	1    2050 6200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #GND_07
U 1 1 5B7596B6
P 1550 6100
F 0 "#GND_07" H 1550 6240 20  0001 C CNN
F 1 "GND" H 1550 6210 30  0001 C CNN
F 2 "" H 1550 6100 70  0000 C CNN
F 3 "" H 1550 6100 70  0000 C CNN
	1    1550 6100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #GND_08
U 1 1 5B7596B5
P 2750 6100
F 0 "#GND_08" H 2750 6240 20  0001 C CNN
F 1 "GND" H 2750 6210 30  0001 C CNN
F 2 "" H 2750 6100 70  0000 C CNN
F 3 "" H 2750 6100 70  0000 C CNN
	1    2750 6100
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 5800 1550 5700
Wire Wire Line
	2750 5800 2750 5700
$Comp
L power:GND #GND_09
U 1 1 5B7596B4
P 8450 4900
F 0 "#GND_09" H 8450 5040 20  0001 C CNN
F 1 "GND" H 8450 5010 30  0001 C CNN
F 2 "" H 8450 4900 70  0000 C CNN
F 3 "" H 8450 4900 70  0000 C CNN
	1    8450 4900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #GND_010
U 1 1 5B7596B2
P 6900 4800
F 0 "#GND_010" H 6900 4940 20  0001 C CNN
F 1 "GND" H 6900 4910 30  0001 C CNN
F 2 "" H 6900 4800 70  0000 C CNN
F 3 "" H 6900 4800 70  0000 C CNN
	1    6900 4800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #GND_011
U 1 1 5B7596B1
P 4750 5400
F 0 "#GND_011" H 4750 5540 20  0001 C CNN
F 1 "GND" H 4750 5510 30  0001 C CNN
F 2 "" H 4750 5400 70  0000 C CNN
F 3 "" H 4750 5400 70  0000 C CNN
	1    4750 5400
	1    0    0    -1  
$EndComp
NoConn ~ 3200 2400
NoConn ~ 3200 2500
NoConn ~ 3200 2600
NoConn ~ 3200 2700
$Comp
L GLCD-controler-SchDoc-rescue:VCC #VCC_03
U 1 1 5B7596B0
P 4900 3000
F 0 "#VCC_03" H 4900 3000 20  0001 C CNN
F 1 "VCC" H 4900 3150 30  0000 C CNN
F 2 "" H 4900 3000 70  0000 C CNN
F 3 "" H 4900 3000 70  0000 C CNN
	1    4900 3000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #GND_012
U 1 1 5B7596AF
P 4900 3800
F 0 "#GND_012" H 4900 3940 20  0001 C CNN
F 1 "GND" H 4900 3910 30  0001 C CNN
F 2 "" H 4900 3800 70  0000 C CNN
F 3 "" H 4900 3800 70  0000 C CNN
	1    4900 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	1200 2000 1700 2000
Wire Wire Line
	1200 1300 1200 1500
NoConn ~ 7550 3700
NoConn ~ 7550 4100
NoConn ~ 7550 4500
Connection ~ 1200 1600
Connection ~ 1200 1500
Connection ~ 8000 2200
Connection ~ 1550 5700
Connection ~ 2750 5700
$Comp
L Connector_Generic:Conn_02x05_Odd_Even EXP1
U 1 1 5B7596AE
P 2250 3900
F 0 "EXP1" H 2400 3600 60  0000 R TNN
F 1 "Header 5X2" H 2650 4000 60  0001 R TNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 2650 4000 60  0001 C CNN
F 3 "https://s3.amazonaws.com/catalogspreads-pdf/PAGE110-111%20.100%20MALE%20HDR%20ST.pdf" H 2650 4000 60  0001 C CNN
F 4 "Sullins Connector Solutions" H -150 -1100 50  0001 C CNN "Manufacturer"
F 5 "PRPC005DAAN-RC" H -150 -1100 50  0001 C CNN "Mfr PN"
F 6 "1" H -150 -1100 50  0001 C CNN "Qty Per Unit"
F 7 "S2011EC-05-ND	" H -150 -1100 50  0001 C CNN "Vendor 1 PN"
F 8 "https://www.digikey.com/products/en?keywords=S2011EC-05-ND" H -150 -1100 50  0001 C CNN "Vendor 1 URL"
F 9 "y" H -150 -1100 50  0001 C CNN "Subs OK?"
F 10 "CONN HEADER .100\" DUAL STR 10POS" H 0   0   50  0001 C CNN "Description"
	1    2250 3900
	-1   0    0    1   
$EndComp
$Comp
L mic5209:MIC5209 U2
U 1 1 5B7596AC
P 2150 5800
F 0 "U2" H 2000 6050 60  0000 L BNN
F 1 "MIC5209" H 2300 5500 60  0000 L BNN
F 2 "Package_TO_SOT_SMD:SOT-223" H 2150 5900 60  0001 C CNN
F 3 "http://www.microchip.com/mymicrochip/filehandler.aspx?ddocname=en579634" H 2150 5900 60  0001 C CNN
F 4 "Microchip Technology" H -6350 4400 50  0001 C CNN "Manufacturer"
F 5 "MIC5209-3.3YS-TR" H -6350 4400 50  0001 C CNN "Mfr PN"
F 6 "1" H -6350 4400 50  0001 C CNN "Qty Per Unit"
F 7 "576-1274-1-ND" H -6350 4400 50  0001 C CNN "Vendor 1 PN"
F 8 "https://www.digikey.com/products/en?keywords=576-1274-1-ND" H -6350 4400 50  0001 C CNN "Vendor 1 URL"
F 9 "SOT-223" H -6350 4400 50  0001 C CNN "JEDEC Pkg"
F 10 "y" H -6350 4400 50  0001 C CNN "Subs OK?"
F 11 "IC REG LINEAR 3.3V 500MA SOT223" H 0   0   50  0001 C CNN "Description"
	1    2150 5800
	1    0    0    -1  
$EndComp
$Comp
L GLCD-controler-SchDoc-rescue:Cap_Semi C1
U 1 1 5B7596AB
P 1450 6000
F 0 "C1" V 1460 5810 60  0000 R TNN
F 1 "0.1uF" V 1360 5810 60  0000 R TNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1360 5810 60  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/UPY-GPHC_Y5V_6.3V-to-50V_8.pdf" H 1360 5810 60  0001 C CNN
F 4 "0805" H -6550 4300 50  0001 C CNN "JEDEC Pkg"
F 5 "1" H -6550 4300 50  0001 C CNN "Qty Per Unit"
F 6 "y" H -6550 4300 50  0001 C CNN "Subs OK?"
F 7 "Yageo" H 0   0   50  0001 C CNN "Manufacturer"
F 8 "CC0805ZRY5V7BB104" H 0   0   50  0001 C CNN "Mfr PN"
F 9 "311-4354-1-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/products/en?keywords=311-4354-1-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "CAP CER 0.1UF 16V Y5V 0805" H 0   0   50  0001 C CNN "Description"
	1    1450 6000
	0    -1   -1   0   
$EndComp
$Comp
L GLCD-controler-SchDoc-rescue:Cap_Semi C2
U 1 1 5B7596AA
P 2650 6000
F 0 "C2" V 2660 5810 60  0000 R TNN
F 1 "0.1uF" V 2560 5810 60  0000 R TNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 2560 5810 60  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/UPY-GPHC_Y5V_6.3V-to-50V_8.pdf" H 2560 5810 60  0001 C CNN
F 4 "0805" H -6550 4300 50  0001 C CNN "JEDEC Pkg"
F 5 "1" H -6550 4300 50  0001 C CNN "Qty Per Unit"
F 6 "y" H -6550 4300 50  0001 C CNN "Subs OK?"
F 7 "Yageo" H 0   0   50  0001 C CNN "Manufacturer"
F 8 "CC0805ZRY5V7BB104" H 0   0   50  0001 C CNN "Mfr PN"
F 9 "311-4354-1-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/products/en?keywords=311-4354-1-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "CAP CER 0.1UF 16V Y5V 0805" H 0   0   50  0001 C CNN "Description"
	1    2650 6000
	0    -1   -1   0   
$EndComp
$Comp
L GLCD-controler-SchDoc-rescue:Cap_Semi C3
U 1 1 5B7596A9
P 8000 4350
F 0 "C3" V 8100 4400 60  0000 R TNN
F 1 "0.1uF" V 7900 4500 60  0000 R TNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7910 4160 60  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/UPY-GPHC_Y5V_6.3V-to-50V_8.pdf" H 7910 4160 60  0001 C CNN
F 4 "0805" H 1300 -1650 50  0001 C CNN "JEDEC Pkg"
F 5 "1" H 1300 -1650 50  0001 C CNN "Qty Per Unit"
F 6 "y" H 1300 -1650 50  0001 C CNN "Subs OK?"
F 7 "Yageo" H 0   0   50  0001 C CNN "Manufacturer"
F 8 "CC0805ZRY5V7BB104" H 0   0   50  0001 C CNN "Mfr PN"
F 9 "311-4354-1-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/products/en?keywords=311-4354-1-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "CAP CER 0.1UF 16V Y5V 0805" H 0   0   50  0001 C CNN "Description"
	1    8000 4350
	0    -1   -1   0   
$EndComp
$Comp
L GLCD-controler-SchDoc-rescue:Cap_Semi C4
U 1 1 5B7596A8
P 7800 2550
F 0 "C4" V 7900 2600 60  0000 R TNN
F 1 "1uF" V 7700 2650 60  0000 R TNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7710 2360 60  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/mlcc/CL21F105ZOCNNNC.jsp" H 7710 2360 60  0001 C CNN
F 4 "0805" H 300 -2500 50  0001 C CNN "JEDEC Pkg"
F 5 "1" H 300 -2500 50  0001 C CNN "Qty Per Unit"
F 6 "y" H 300 -2500 50  0001 C CNN "Subs OK?"
F 7 "Samsung Electro-Mechanics" H 0   0   50  0001 C CNN "Manufacturer"
F 8 "CL21F105ZOCNNNC" H 0   0   50  0001 C CNN "Mfr PN"
F 9 "1276-1246-1-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/products/en?keywords=1276-1246-1-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "CAP CER 1UF 16V Y5V 0805" H 0   0   50  0001 C CNN "Description"
	1    7800 2550
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push PB1
U 1 1 5B7596A7
P 4750 5150
F 0 "PB1" V 4850 5250 60  0000 L BNN
F 1 "STOP" V 4950 5250 60  0000 L BNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 4950 5250 60  0001 C CNN
F 3 "https://www3.panasonic.biz/ac/e_download/control/switch/light-touch/catalog/sw_lt_eng_5n.pdf" H 4950 5250 60  0001 C CNN
F 4 "1" H -950 -550 50  0001 C CNN "Qty Per Unit"
F 5 "y" H -950 -550 50  0001 C CNN "Subs OK?"
F 6 "Panasonic Electronic Components" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "EVQ-PAG04M" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "P8009S-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 9 "https://www.digikey.com/products/en?keywords=P8009S-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 10 "SWITCH TACTILE SPST-NO 0.02A 15V" H 0   0   50  0001 C CNN "Description"
	1    4750 5150
	0    1    1    0   
$EndComp
$Comp
L Device:Rotary_Encoder_Switch EN1
U 1 1 5B7596A6
P 4900 2000
F 0 "EN1" H 4800 2250 60  0000 L BNN
F 1 "EN11-HSB1AQ20" H 4650 1650 60  0001 L BNN
F 2 "Rotary_Encoder:RotaryEncoder_Alps_EC11E-Switch_Vertical_H20mm" H 5300 2100 60  0001 C CNN
F 3 "http://www.ttelectronics.com/sites/default/files/download-files/Datasheet_RotaryEncoder_EN11Series.pdf" H 5300 2100 60  0001 C CNN
F 4 "TT Electronics/BI" H 2900 -4300 50  0001 C CNN "Manufacturer"
F 5 "EN11-HSB1AQ20" H 2900 -4300 50  0001 C CNN "Mfr PN"
F 6 "1" H 2900 -4300 50  0001 C CNN "Qty Per Unit"
F 7 "987-1187-ND" H 2900 -4300 50  0001 C CNN "Vendor 1 PN"
F 8 "https://www.digikey.com/products/en?keywords=987-1187-ND" H 2900 -4300 50  0001 C CNN "Vendor 1 URL"
F 9 "y" H 2900 -4300 50  0001 C CNN "Subs OK?"
F 10 "ENCODER 11MM ROTARY SW TOP ADJ" H 0   0   50  0001 C CNN "Description"
	1    4900 2000
	1    0    0    -1  
$EndComp
$Comp
L GLCD-controler-SchDoc-rescue:LCD_KXM1264M-3 GLCD1
U 1 1 5B7596A5
P 2450 2100
F 0 "GLCD1" H 2600 2850 60  0000 R BNN
F 1 "KXM12864M-3" H 2150 1300 60  0000 L BNN
F 2 "12864-lcd:KXM12864M-3" H 2150 1300 60  0001 C CNN
F 3 "" H 2150 1300 60  0000 C CNN
F 4 "1" H 150 0   50  0001 C CNN "Qty Per Unit"
F 5 "y" H 150 0   50  0001 C CNN "Subs OK?"
F 6 "B073WBMLWB" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 7 "https://www.amazon.com/gp/product/B073WBMLWB?pf_rd_p=d1f45e03-8b73-4c9a-9beb-4819111bef9a&pf_rd_r=EWAG3W4BYX38GDBGJ7CV" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 8 "SODIAL(R) ST7920 128x64 12864 LCD Display Blue Backlight Parallel Serial Module for Arduino 5V" H 0   0   50  0001 C CNN "Description"
	1    2450 2100
	-1   0    0    -1  
$EndComp
$Comp
L Device:Q_NPN_BEC Q1
U 1 1 5B7596A2
P 4800 3600
F 0 "Q1" H 5000 3650 60  0000 L BNN
F 1 "MMBT2222" H 5000 3550 60  0000 L BNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 4930 3410 60  0001 C CNN
F 3 "http://www.onsemi.com/pub/Collateral/MMBT2222LT1-D.PDF" H 4930 3410 60  0001 C CNN
F 4 "ON Semiconductor" H -200 1100 50  0001 C CNN "Manufacturer"
F 5 "MMBT2222LT1G" H -200 1100 50  0001 C CNN "Mfr PN"
F 6 "1" H -200 1100 50  0001 C CNN "Qty Per Unit"
F 7 "MMBT2222LT1GOSCT-ND" H -200 1100 50  0001 C CNN "Vendor 1 PN"
F 8 "https://www.digikey.com/products/en?keywords=MMBT2222LT1GOSCT-ND" H -200 1100 50  0001 C CNN "Vendor 1 URL"
F 9 "SOT-23-3" H -200 1100 50  0001 C CNN "JEDEC Pkg"
F 10 "y" H -200 1100 50  0001 C CNN "Subs OK?"
F 11 "TRANS NPN 30V 0.6A SOT23" H 0   0   50  0001 C CNN "Description"
	1    4800 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R1
U 1 1 5B7596A1
P 4450 3600
F 0 "R1" V 4550 3650 60  0000 R TNN
F 1 "1K" V 4300 3650 60  0000 R TNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 4760 3640 60  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_9.pdf" H 4760 3640 60  0001 C CNN
F 4 "R" V 1950 2600 60  0001 C CNN "Spice Prefix"
F 5 "0805" H -100 1000 50  0001 C CNN "JEDEC Pkg"
F 6 "1" H -100 1000 50  0001 C CNN "Qty Per Unit"
F 7 "y" H -100 1000 50  0001 C CNN "Subs OK?"
F 8 "Yageo" H 0   0   50  0001 C CNN "Manufacturer"
F 9 "RC0805FR-071KL" H 0   0   50  0001 C CNN "Mfr PN"
F 10 "311-1.00KCRCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 11 "https://www.digikey.com/products/en?keywords=311-1.00KCRCT-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 12 "RES SMD 1K OHM 1% 1/8W 0805" H 0   0   50  0001 C CNN "Description"
	1    4450 3600
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_US RBL1
U 1 1 5B7596A0
P 1500 1500
F 0 "RBL1" V 1550 1600 60  0000 R TNN
F 1 "0" V 1400 1400 60  0000 R TNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 1800 1300 60  0001 C CNN
F 3 "https://www2.mouser.com/datasheet/2/427/dcrcwe3-109170.pdf" H 1800 1300 60  0001 C CNN
F 4 "R" V 2100 1600 60  0001 C CNN "Spice Prefix"
F 5 "0805" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 6 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 7 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 8 "Vishay" H 0   0   50  0001 C CNN "Manufacturer"
F 9 "CRCW08050000Z0EA" H 0   0   50  0001 C CNN "Mfr PN"
F 10 "71-CRCW0805-0-E3" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 11 "https://www.mouser.com/ProductDetail/Vishay/CRCW08050000Z0EA?qs=sGAEpiMZZMtlubZbdhIBIAPdxL7UgFqoWM4Gq0J8%252bCE=" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 12 "Thick Film Resistors - SMD 1/8watt ZEROohm Jumper" H 0   0   50  0001 C CNN "Description"
	1    1500 1500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1200 1600 1200 2000
Wire Wire Line
	1200 1500 1200 1600
Wire Wire Line
	8000 2200 8000 1900
$Comp
L 2041021-4:2041021-4 SD1
U 1 1 5B7646B0
P 8500 2300
F 0 "SD1" H 8270 2250 50  0000 R CNN
F 1 "2041021-4" H 8270 2205 50  0001 R CNN
F 2 "2041021-4:J_SMD_2041021-4-9_2.5_27.90_25_3.3" H 8500 2300 50  0001 L BNN
F 3 "http://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=2041021&DocType=Customer+Drawing&DocLang=English" H 8500 2300 50  0001 L BNN
F 4 "TE Connectivity" H 8500 2300 50  0001 C CNN "Manufacturer"
F 5 "2041021-4" H 8500 2300 50  0001 C CNN "Mfr PN"
F 6 "1" H 8500 2300 50  0001 C CNN "Qty Per Unit"
F 7 "A101492CT-ND" H 8500 2300 50  0001 C CNN "Vendor 1 PN"
F 8 "https://www.digikey.com/products/en?keywords=A101492CT-ND" H 8500 2300 50  0001 C CNN "Vendor 1 URL"
F 9 "y" H 8500 2300 50  0001 C CNN "Subs OK?"
F 10 "CONN SD CARD PUSH-PULL R/A SMD	" H 0   0   50  0001 C CNN "Description"
	1    8500 2300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7900 2000 7900 2350
Connection ~ 7900 2000
Wire Wire Line
	8000 2200 8000 3000
Wire Wire Line
	8200 3000 8000 3000
Connection ~ 8000 3000
Wire Wire Line
	8000 3000 8000 3100
Wire Wire Line
	8000 3000 7900 3000
Wire Wire Line
	7900 3000 7900 2650
Wire Wire Line
	1200 1600 1700 1600
Wire Wire Line
	1200 1500 1350 1500
Wire Wire Line
	1650 1500 1700 1500
$Comp
L CPE-164:CPE-164 PZ1
U 1 1 5B77EB3F
P 4900 3200
F 0 "PZ1" H 5004 3246 50  0000 L CNN
F 1 "CPE-164" H 5004 3155 50  0000 L CNN
F 2 "CPE-164:CUI_CPE-164" H 4900 3200 50  0001 L BNN
F 3 "https://www.cui.com/product/resource/digikeypdf/cpe-164.pdf" H 4900 3200 50  0001 L BNN
F 4 "CUI" H 4900 3200 50  0001 C CNN "Manufacturer"
F 5 "CPE-164" H 4900 3200 50  0001 C CNN "Mfr PN"
F 6 "1" H 4900 3200 50  0001 C CNN "Qty Per Unit"
F 7 "102-1612-ND" H 4900 3200 50  0001 C CNN "Vendor 1 PN"
F 8 "https://www.digikey.com/product-detail/en/cui-inc/CPE-164/102-1612-ND/1738886" H 4900 3200 50  0001 C CNN "Vendor 1 URL"
F 9 "y" H 4900 3200 50  0001 C CNN "Subs OK?"
F 10 "AUDIO PIEZO TRANSDUCER 20V TH" H 0   0   50  0001 C CNN "Description"
	1    4900 3200
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4050 U1
U 1 1 5B780158
P 7250 3700
F 0 "U1" H 7300 3850 50  0000 C CNN
F 1 "74HC4050" H 7250 3926 50  0001 C CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 7250 3700 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/cd74hc4049.pdf" H 7250 3700 50  0001 C CNN
F 4 "Texas Instruments" H 250 1300 50  0001 C CNN "Manufacturer"
F 5 "CD74HC4050M96" H 250 1300 50  0001 C CNN "Mfr PN"
F 6 "1" H 250 1300 50  0001 C CNN "Qty Per Unit"
F 7 "296-14529-1-ND" H 250 1300 50  0001 C CNN "Vendor 1 PN"
F 8 "https://www.digikey.com/products/en?keywords=296-14529-1-ND" H 250 1300 50  0001 C CNN "Vendor 1 URL"
F 9 "SOIC-16" H 250 1300 50  0001 C CNN "JEDEC Pkg"
F 10 "y" H 250 1300 50  0001 C CNN "Subs OK?"
F 11 "IC BUFFER NON-INVERT 6V 16SOIC" H 0   0   50  0001 C CNN "Description"
	1    7250 3700
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4050 U1
U 2 1 5B780297
P 7250 4500
F 0 "U1" H 7300 4650 50  0000 C CNN
F 1 "74HC4050" H 7250 4726 50  0001 C CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 7250 4500 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/cd74hc4049.pdf" H 7250 4500 50  0001 C CNN
F 4 "16-SOIC" H 250 1300 50  0001 C CNN "JEDEC Pkg"
F 5 "Texas Instruments" H 250 1300 50  0001 C CNN "Manufacturer"
F 6 "CD74HC4050M96" H 250 1300 50  0001 C CNN "Mfr PN"
F 7 "1" H 250 1300 50  0001 C CNN "Qty Per Unit"
F 8 "y" H 250 1300 50  0001 C CNN "Subs OK?"
F 9 "296-14529-1-ND" H 250 1300 50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/products/en?keywords=296-14529-1-ND" H 250 1300 50  0001 C CNN "Vendor 1 URL"
F 11 "IC BUFFER NON-INVERT 6V 16SOIC" H 0   0   50  0001 C CNN "Description"
	2    7250 4500
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4050 U1
U 3 1 5B78033C
P 7250 4100
F 0 "U1" H 7300 4250 50  0000 C CNN
F 1 "74HC4050" H 7250 4326 50  0001 C CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 7250 4100 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/cd74hc4049.pdf" H 7250 4100 50  0001 C CNN
F 4 "16-SOIC" H 250 1300 50  0001 C CNN "JEDEC Pkg"
F 5 "Texas Instruments" H 250 1300 50  0001 C CNN "Manufacturer"
F 6 "CD74HC4050M96" H 250 1300 50  0001 C CNN "Mfr PN"
F 7 "1" H 250 1300 50  0001 C CNN "Qty Per Unit"
F 8 "y" H 250 1300 50  0001 C CNN "Subs OK?"
F 9 "296-14529-1-ND" H 250 1300 50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/products/en?keywords=296-14529-1-ND" H 250 1300 50  0001 C CNN "Vendor 1 URL"
F 11 "IC BUFFER NON-INVERT 6V 16SOIC" H 0   0   50  0001 C CNN "Description"
	3    7250 4100
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4050 U1
U 4 1 5B78102E
P 7000 2100
F 0 "U1" H 7050 2250 50  0000 C CNN
F 1 "74HC4050" H 7000 2326 50  0001 C CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 7000 2100 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/cd74hc4049.pdf" H 7000 2100 50  0001 C CNN
F 4 "16-SOIC" H 0   -2800 50  0001 C CNN "JEDEC Pkg"
F 5 "Texas Instruments" H 0   -2800 50  0001 C CNN "Manufacturer"
F 6 "CD74HC4050M96" H 0   -2800 50  0001 C CNN "Mfr PN"
F 7 "1" H 0   -2800 50  0001 C CNN "Qty Per Unit"
F 8 "y" H 0   -2800 50  0001 C CNN "Subs OK?"
F 9 "296-14529-1-ND" H 0   -2800 50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/products/en?keywords=296-14529-1-ND" H 0   -2800 50  0001 C CNN "Vendor 1 URL"
F 11 "IC BUFFER NON-INVERT 6V 16SOIC" H 0   0   50  0001 C CNN "Description"
	4    7000 2100
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4050 U1
U 5 1 5B78109B
P 7000 1750
F 0 "U1" H 7050 1900 50  0000 C CNN
F 1 "74HC4050" H 7000 1976 50  0001 C CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 7000 1750 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/cd74hc4049.pdf" H 7000 1750 50  0001 C CNN
F 4 "16-SOIC" H 0   -2750 50  0001 C CNN "JEDEC Pkg"
F 5 "Texas Instruments" H 0   -2750 50  0001 C CNN "Manufacturer"
F 6 "CD74HC4050M96" H 0   -2750 50  0001 C CNN "Mfr PN"
F 7 "1" H 0   -2750 50  0001 C CNN "Qty Per Unit"
F 8 "y" H 0   -2750 50  0001 C CNN "Subs OK?"
F 9 "296-14529-1-ND" H 0   -2750 50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/products/en?keywords=296-14529-1-ND" H 0   -2750 50  0001 C CNN "Vendor 1 URL"
F 11 "IC BUFFER NON-INVERT 6V 16SOIC" H 0   0   50  0001 C CNN "Description"
	5    7000 1750
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4050 U1
U 6 1 5B781118
P 7000 1400
F 0 "U1" H 7050 1550 50  0000 C CNN
F 1 "74HC4050" H 7000 1626 50  0001 C CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 7000 1400 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/cd74hc4049.pdf" H 7000 1400 50  0001 C CNN
F 4 "16-SOIC" H 0   -2700 50  0001 C CNN "JEDEC Pkg"
F 5 "Texas Instruments" H 0   -2700 50  0001 C CNN "Manufacturer"
F 6 "CD74HC4050M96" H 0   -2700 50  0001 C CNN "Mfr PN"
F 7 "1" H 0   -2700 50  0001 C CNN "Qty Per Unit"
F 8 "y" H 0   -2700 50  0001 C CNN "Subs OK?"
F 9 "296-14529-1-ND" H 0   -2700 50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/products/en?keywords=296-14529-1-ND" H 0   -2700 50  0001 C CNN "Vendor 1 URL"
F 11 "IC BUFFER NON-INVERT 6V 16SOIC" H 0   0   50  0001 C CNN "Description"
	6    7000 1400
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4050 U1
U 7 1 5B7811C7
P 8450 4300
F 0 "U1" H 8680 4300 50  0000 L CNN
F 1 "74HC4050" H 8680 4255 50  0001 L CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 8450 4300 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/cd74hc4049.pdf" H 8450 4300 50  0001 C CNN
F 4 "16-SOIC" H 250 1300 50  0001 C CNN "JEDEC Pkg"
F 5 "Texas Instruments" H 250 1300 50  0001 C CNN "Manufacturer"
F 6 "CD74HC4050M96" H 250 1300 50  0001 C CNN "Mfr PN"
F 7 "1" H 250 1300 50  0001 C CNN "Qty Per Unit"
F 8 "y" H 250 1300 50  0001 C CNN "Subs OK?"
F 9 "296-14529-1-ND" H 250 1300 50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/products/en?keywords=296-14529-1-ND" H 250 1300 50  0001 C CNN "Vendor 1 URL"
F 11 "IC BUFFER NON-INVERT 6V 16SOIC" H 0   0   50  0001 C CNN "Description"
	7    8450 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 4800 6900 4500
Wire Wire Line
	6900 4500 6950 4500
Wire Wire Line
	6900 4500 6900 4100
Wire Wire Line
	6900 4100 6950 4100
Connection ~ 6900 4500
Wire Wire Line
	6900 4100 6900 3700
Wire Wire Line
	6900 3700 6950 3700
Connection ~ 6900 4100
Wire Wire Line
	8450 4800 8450 4850
Wire Wire Line
	8450 4850 8100 4850
Wire Wire Line
	8100 4850 8100 4450
Connection ~ 8450 4850
Wire Wire Line
	8450 4850 8450 4900
Wire Wire Line
	8450 3700 8450 3750
Wire Wire Line
	8450 3750 8100 3750
Wire Wire Line
	8100 3750 8100 4150
Connection ~ 8450 3750
Wire Wire Line
	8450 3750 8450 3800
Text GLabel 1950 3800 0    50   UnSpc ~ 0
LCD7
Text GLabel 1950 3900 0    50   UnSpc ~ 0
LCD5
Text GLabel 1950 4000 0    50   UnSpc ~ 0
LCDRS
Text GLabel 1950 4100 0    50   UnSpc ~ 0
BTN_ENC
Text GLabel 2450 3800 2    50   UnSpc ~ 0
LCD6
Text GLabel 2450 3900 2    50   UnSpc ~ 0
LCD4
Text GLabel 2450 4000 2    50   UnSpc ~ 0
LCDE
Text GLabel 2450 4100 2    50   UnSpc ~ 0
Beeper
$Comp
L Connector_Generic:Conn_02x05_Odd_Even EXP2
U 1 1 5B7A4EA6
P 2150 4650
F 0 "EXP2" H 2300 5000 60  0000 R TNN
F 1 "Header 5X2" H 2550 4750 60  0001 R TNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 2550 4750 60  0001 C CNN
F 3 "https://s3.amazonaws.com/catalogspreads-pdf/PAGE110-111%20.100%20MALE%20HDR%20ST.pdf" H 2550 4750 60  0001 C CNN
F 4 "Sullins Connector Solutions" H -250 -350 50  0001 C CNN "Manufacturer"
F 5 "PRPC005DAAN-RC" H -250 -350 50  0001 C CNN "Mfr PN"
F 6 "1" H -250 -350 50  0001 C CNN "Qty Per Unit"
F 7 "S2011EC-05-ND	" H -250 -350 50  0001 C CNN "Vendor 1 PN"
F 8 "https://www.digikey.com/products/en?keywords=S2011EC-05-ND" H -250 -350 50  0001 C CNN "Vendor 1 URL"
F 9 "y" H -250 -350 50  0001 C CNN "Subs OK?"
F 10 "CONN HEADER .100\" DUAL STR 10POS" H 0   0   50  0001 C CNN "Description"
	1    2150 4650
	1    0    0    -1  
$EndComp
Text GLabel 1950 4450 0    50   UnSpc ~ 0
PB3_(MISO)
Text GLabel 1950 4550 0    50   UnSpc ~ 0
BTN_EN2
Text GLabel 1950 4650 0    50   UnSpc ~ 0
BTN_EN1
Text GLabel 1950 4750 0    50   UnSpc ~ 0
SD_DET
Text GLabel 2450 4450 2    50   UnSpc ~ 0
PB1_(SCK)
Text GLabel 2450 4550 2    50   UnSpc ~ 0
SD_CSEL
Text GLabel 2450 4650 2    50   UnSpc ~ 0
PB2_(MOSI)
Text GLabel 2450 4750 2    50   UnSpc ~ 0
RESET
Text GLabel 2450 4850 2    50   UnSpc ~ 0
KILL
Text GLabel 6700 1400 0    50   UnSpc ~ 0
SD_CSEL
Text GLabel 6700 1750 0    50   UnSpc ~ 0
PB2_(MOSI)
Text GLabel 6700 2100 0    50   UnSpc ~ 0
PB1_(SCK)
Text GLabel 4750 4900 1    50   UnSpc ~ 0
RESET
Text GLabel 4500 1900 0    50   UnSpc ~ 0
BTN_EN1
$Comp
L power:GND #GND_0101
U 1 1 5B7AC013
P 5300 2100
F 0 "#GND_0101" H 5300 2240 20  0001 C CNN
F 1 "GND" H 5300 2210 30  0001 C CNN
F 2 "" H 5300 2100 70  0000 C CNN
F 3 "" H 5300 2100 70  0000 C CNN
	1    5300 2100
	1    0    0    -1  
$EndComp
Text GLabel 4500 2100 0    50   UnSpc ~ 0
BTN_EN2
$Comp
L power:GND #GND_0102
U 1 1 5B7AC127
P 3950 2100
F 0 "#GND_0102" H 3950 2240 20  0001 C CNN
F 1 "GND" H 3950 2210 30  0001 C CNN
F 2 "" H 3950 2100 70  0000 C CNN
F 3 "" H 3950 2100 70  0000 C CNN
	1    3950 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 2000 3950 2100
Text GLabel 5300 1900 2    50   UnSpc ~ 0
BTN_ENC
Wire Wire Line
	8200 2700 6700 2700
Text GLabel 6700 2700 0    50   UnSpc ~ 0
SD_DET
Text GLabel 6700 2300 0    50   UnSpc ~ 0
PB3_(MISO)
Wire Wire Line
	6700 2300 8200 2300
Wire Wire Line
	7300 2100 8200 2100
Wire Wire Line
	8200 1800 7650 1800
Wire Wire Line
	7650 1800 7650 1750
Wire Wire Line
	7650 1750 7300 1750
Wire Wire Line
	8200 1700 7750 1700
Wire Wire Line
	7750 1700 7750 1400
Wire Wire Line
	7750 1400 7300 1400
Text GLabel 3200 1500 2    50   UnSpc ~ 0
LCDRS
Text GLabel 3200 1600 2    50   UnSpc ~ 0
LCDE
Text GLabel 3200 1700 2    50   UnSpc ~ 0
LCD4
$Comp
L GLCD-controler-SchDoc-rescue:VCC #VCC0102
U 1 1 5B7BBAE5
P 1950 3700
F 0 "#VCC0102" H 1950 3700 20  0001 C CNN
F 1 "VCC" H 1950 3850 30  0000 C CNN
F 2 "" H 1950 3700 70  0000 C CNN
F 3 "" H 1950 3700 70  0000 C CNN
	1    1950 3700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #GND_0103
U 1 1 5B7BBBC0
P 2750 3700
F 0 "#GND_0103" H 2750 3840 20  0001 C CNN
F 1 "GND" H 2750 3810 30  0001 C CNN
F 2 "" H 2750 3700 70  0000 C CNN
F 3 "" H 2750 3700 70  0000 C CNN
	1    2750 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2450 3700 2750 3700
Text GLabel 4300 3600 0    50   UnSpc ~ 0
Beeper
$Comp
L power:GND #GND_0104
U 1 1 5B7C5BD8
P 2250 6200
F 0 "#GND_0104" H 2250 6340 20  0001 C CNN
F 1 "GND" H 2250 6310 30  0001 C CNN
F 2 "" H 2250 6200 70  0000 C CNN
F 3 "" H 2250 6200 70  0000 C CNN
	1    2250 6200
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 2100 1700 2600
Wire Wire Line
	1700 2600 1700 2700
Connection ~ 1700 2600
Wire Wire Line
	1700 2700 1700 2800
Connection ~ 1700 2700
Wire Wire Line
	4750 4900 4750 4950
Wire Wire Line
	4750 5350 4750 5400
Wire Wire Line
	4500 1900 4600 1900
Wire Wire Line
	3950 2000 4600 2000
Wire Wire Line
	4500 2100 4600 2100
Wire Wire Line
	5200 1900 5300 1900
Wire Wire Line
	5300 2100 5200 2100
$EndSCHEMATC
