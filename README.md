ReprapDiscount Full Graphic LCD Controller
==========================================

This is the ReprapDiscount Full Graphic LCD Controller:

http://forum.reprapdiscount.com/threads/full-graphics-smart-controller-open-source-files.619/

I took their Altium Designer files, ran them through an automated
Altium-to-KiCad conversion (http://www2.futureware.at/KiCad/), and cleaned
up the files.  Gerbers have been generated.  Footprints have been updated to
the ones provided by KiCad (the main difference is that pin 1 for the
various header connectors is square or rectangular instead of round or
oval).  Components also have part numbers and datasheets attached for BOM
generation.

This project is probably more about liberating open hardware from closed EDA
than anything else, and as a simple test, it succeeded. :)

The original Altium files are in the originals directory for verification
purposes.